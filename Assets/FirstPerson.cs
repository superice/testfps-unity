﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CharacterController))]
public class FirstPerson : MonoBehaviour {

	// These are actually useful
	public float movementSpeed = 5.0f;	
	public float rotationSpeed = 3.0f;
	public float pitchRange = 70.0f;
	public float jumpSpeed = 3.5f;

	// This one is just for initializing
	public float rotPitch = 0;
	public float verticalVelocity = 0;
	public CharacterController cc;

	void Start () {
		// Input new data into the character
		cc = GetComponent<CharacterController> ();

		// Make sure we don't get the ugly-ass cursor
		Screen.lockCursor = true;
	}

	void Update () {
		updateRotation ();
		updateSpeed ();
	}

	void startJump() {
		if (Input.GetButton ("Jump") && cc.isGrounded) {
			verticalVelocity = jumpSpeed;
			verticalVelocity = jumpSpeed;
		}
	}

	void updateRotation() {
		// Fix x-rotation
		float rotYaw = Input.GetAxis ("Mouse X") * rotationSpeed;
		transform.Rotate (0, rotYaw, 0);

		// Fix y-rotation	
		rotPitch -= Input.GetAxis ("Mouse Y") * rotationSpeed;
		rotPitch = Mathf.Clamp (rotPitch, -pitchRange, pitchRange);
		Camera.main.transform.localRotation = Quaternion.Euler (rotPitch, 0, 0);
	}

	void updateSpeed() {
		// Get the movement speed
		float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
		float sideSpeed = Input.GetAxis ("Horizontal") * movementSpeed;

		// Add gravity
		if (cc.isGrounded == false) {
			verticalVelocity += Physics.gravity.y * Time.deltaTime;
			if (cc.isGrounded == true) {
				verticalVelocity = 0;
			}
		} else {
			verticalVelocity = 0;
		}

		// Check for jump
		startJump ();

		Vector3 speed = new Vector3 (sideSpeed, verticalVelocity, forwardSpeed);

		// Apply rotation to movement vector
		speed = transform.rotation * speed;

		cc.Move(speed * Time.deltaTime);
	}
}
